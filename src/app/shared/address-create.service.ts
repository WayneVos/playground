import {Injectable} from '@angular/core';

import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class AddressCreateService {

    constructor(
        private firestore: AngularFirestore
    ) {
    }

    create_NewUser(record) {
        return this.firestore.collection('Users').add(record);
    }

    read_User() {
        return this.firestore.collection('Users').snapshotChanges();
    }

    update_User(recordID, record) {
        this.firestore.doc('Users/' + recordID).update(record);
    }

    delete_User(recordId) {
        this.firestore.doc('Users/' + recordId).delete();
    }

}
