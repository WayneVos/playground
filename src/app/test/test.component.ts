import {Component, OnInit} from '@angular/core';
import {AddressCreateService} from '../shared/address-create.service';

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
    title = 'User Profile';

    users: any;
    name: string;
    lastName: string;
    email: string;
    number: string;

    constructor(
        private crudService: AddressCreateService,
    ) {
    }

    ngOnInit() {
        this.crudService.read_User().subscribe(data => {

            this.users = data.map(e => {
                return {
                    id: e.payload.doc.id,
                    isEdit: false,
                    name: e.payload.doc.data()['name'],
                    lastName: e.payload.doc.data()['lastName'],
                    email: e.payload.doc.data()['email'],
                    number: e.payload.doc.data()['number'],
                };
            });

        });

    }

    CreateRecord() {
        let record = {};
        record['name'] = this.name;
        record['lastName'] = this.lastName;
        record['email'] = this.email;
        record['number'] = this.number;
        this.crudService.create_NewUser(record).then(resp => {
            this.name = '';
            this.lastName = '';
            this.email = '';
            this.number = '';
        })
            .catch(error => {
                console.log(error);
            });
    }

    RemoveRecord(rowID) {
        this.crudService.delete_User(rowID);
    }

    EditRecord(record) {
        record.isEdit = true;
        record.EditName = record.name;
        record.EditlastName = record.lastName;
        record.EditEmail = record.email;
        record.EditNumber = record.number;
    }

    UpdateRecord(recordRow) {
        let record = {};
        record['name'] = recordRow.EditName;
        record['lastName'] = recordRow.lastName;
        record['email'] = recordRow.EditEmail;
        record['number'] = recordRow.EditNumber;
        this.crudService.update_User(recordRow.id, record);
        recordRow.isEdit = false;
    }

}
