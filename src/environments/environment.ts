// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBb01lcIuRqleYbEF43VIJ-v0Ip3u4Kslg',
        authDomain: 'testproject-d0d41.firebaseapp.com',
        databaseURL: 'https://testproject-d0d41.firebaseio.com',
        projectId: 'testproject-d0d41',
        storageBucket: 'testproject-d0d41.appspot.com',
        messagingSenderId: '551102685775',
        appId: '1:551102685775:web:63273e66babe9574'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
